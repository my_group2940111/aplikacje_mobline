package com.example.qrmaster;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
public class ListAdapter extends ArrayAdapter<String> {
    Context context;
    List<String> qrCodes;
    List<String> qrTitles;

    public ListAdapter(Context context, List<String> qrCodes, List<String> qrTitles) {
        super(context, R.layout.list_codes, qrCodes);
        this.context = context;
        this.qrCodes = qrCodes;
        this.qrTitles = qrTitles;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("ViewHolder") View rowView = inflater.inflate(R.layout.list_codes, parent, false);
        TextView textViewQrCode = (TextView) rowView.findViewById(R.id.urlCode);
        TextView textViewQrTitle = (TextView) rowView.findViewById(R.id.titleCode);
            textViewQrCode.setText(qrCodes.get(position));
            textViewQrTitle.setText(qrTitles.get(position));
        return rowView;
    }
}
