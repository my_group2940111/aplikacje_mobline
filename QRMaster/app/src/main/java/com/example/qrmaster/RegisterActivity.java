package com.example.qrmaster;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {

    EditText username, Email, password, rePassword;
    Button registerConfirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username=findViewById(R.id.usernameEditText);
        password=findViewById(R.id.passwordEditText);
        Email=findViewById(R.id.emailEditText);
        rePassword = findViewById(R.id.rePasswordEditText);
        registerConfirm=findViewById(R.id.registerConfirmButton);
        registerConfirm.setOnClickListener(v -> {
            String user = username.getText().toString();
            String email = Email.getText().toString();
            String pass = password.getText().toString();
            String repass = rePassword.getText().toString();
            if(TextUtils.isEmpty(user) || TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)|| TextUtils.isEmpty(repass))
                Toast.makeText(RegisterActivity.this, "All fields are required", Toast.LENGTH_SHORT).show();
            else{
                if(pass.equals(repass)){
                    Boolean checkUsernameinServer = DBHelper.checkUsernameInServer(user);
                    if(!checkUsernameinServer){
                        Boolean insertServer = DBHelper.insertDataIntoServer(user, email,pass,repass);
                        if(insertServer){
                            Toast.makeText(getBaseContext(), user + " registered successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(this, StartAcitivity.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(RegisterActivity.this, user+" registration failed", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(RegisterActivity.this, user+" already exists", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(RegisterActivity.this, "Passwords are not matching", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}