package com.example.qrmaster;

import android.util.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBHelper{



    public static Boolean insertDataIntoServer(String username, String email, String password, String repassword){
        Connection connection = new ConnectToServer().doInBackground();
        String query = "INSERT INTO users (username, email, password, repassword) VALUES (?, ?, ?, ?)";
            try {
                PreparedStatement ps = connection.prepareStatement(query);
                ps.setString(1, username);
                ps.setString(2, email);
                ps.setString(3,password);
                ps.setString(4,repassword);
                ps.executeUpdate();
            }catch(SQLException e){
                e.printStackTrace();
            }
        return true;
    }

    public static Boolean checkUsernameInServer(String username){
        Connection connection = new ConnectToServer().doInBackground();

        String query = "SELECT * FROM users WHERE username = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean checkUsernamePasswordInServer(String username, String password){
        Connection connection = new ConnectToServer().doInBackground();
        String query = "SELECT * FROM users WHERE username = ? AND password = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean insertQRCodeIntoServer(String username, String title, String qrcode){
        Connection connection = new ConnectToServer().doInBackground();
        String query = "INSERT INTO qrcodes (username, title, qrcode) VALUES (?, ?, ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, title);
            ps.setString(3, qrcode);
            int result = ps.executeUpdate();
            if (result > 0) {
                return true;
            }
        }catch(SQLException e){
           e.printStackTrace();
        }
        return false;
    }

    public static Boolean deleteQRCodeFromServer(String title, String qrcode, String username){
        Connection connection = new ConnectToServer().doInBackground();
        String query = "DELETE FROM qrcodes WHERE title = ? AND qrcode =? AND username=?";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, title);
            ps.setString(2,qrcode);
            ps.setString(3,username);
            int result = ps.executeUpdate();
            if (result > 0) {
                return true;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }


    public static void updateQRURL(String qrurl, String user,String title) {
        Connection connection = new ConnectToServer().doInBackground();
        String query = "UPDATE qrcodes SET qrcode=? WHERE username=? and title=?";
        try {
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setString(1, qrurl);
            pstmt.setString(2, user);
            pstmt.setString(3,title);
            pstmt.executeUpdate();
        } catch(SQLException se) {
            se.printStackTrace();
        }
    }

    public static void updateQRTitle(String qrtitle, String user, String qrcode) {
        Connection connection = new ConnectToServer().doInBackground();
        String query = "UPDATE qrcodes SET title=? WHERE username=? and qrcode=?";
        try {
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setString(1, qrtitle);
            pstmt.setString(2, user);
            pstmt.setString(3,qrcode);
            pstmt.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getAllQRCodesFromServer(String username){
        Connection connection = new ConnectToServer().doInBackground();
        List<String> qrCodes = new ArrayList<>();
        String query = "SELECT qrcode FROM qrcodes WHERE username = ?";
        try{
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, username);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            qrCodes.add(rs.getString("qrcode"));
        }}catch(SQLException e){
            e.printStackTrace();
        }
        return  qrCodes;
    }

    public static List<String> getAllQRTitlesFromServer(String username){
        Connection connection = new ConnectToServer().doInBackground();
        List<String> titles = new ArrayList<>();
        String query = "SELECT title FROM qrcodes WHERE username = ?";
        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                titles.add(rs.getString("title"));
            }}catch(SQLException e){
            Log.e("error here 4: ", e.getMessage());
        }
        return titles;
    }
}
