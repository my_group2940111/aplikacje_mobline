package com.example.qrmaster;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button scanButton;
    Button generateButton;
    ListView listView;
    private static final int MY_CAMERA_REQUEST_CODE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scanButton=findViewById(R.id.scanButton);
        generateButton=findViewById(R.id.generateQRCodeButton);
        listView = findViewById(R.id.lisQrCodesView);
        String user = getIntent().getStringExtra("user");
        List<String> qrCodes;
        List<String> qrTitles;

        qrCodes = DBHelper.getAllQRCodesFromServer(user);
        qrTitles = DBHelper.getAllQRTitlesFromServer(user);
        if (qrCodes.isEmpty()){
            DBHelper.insertQRCodeIntoServer(user,"Obciazenie sal na wydziale Inzynierii Srodowiska i Energetyki Politechniki Krakowskiej","http://sale.wis.pk.edu.pl/day.php?year=2023&month=01&day=16&area=1&room=2");
            qrCodes = DBHelper.getAllQRCodesFromServer(user);
            qrTitles = DBHelper.getAllQRTitlesFromServer(user);
        }

        ListAdapter adapter = new ListAdapter(this, qrCodes, qrTitles);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        List<String> finalQrTitles = qrTitles;
        List<String> finalQrCodes = qrCodes;
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(getApplicationContext(), ChangesActivity.class);
            String title = finalQrTitles.get(position);
            String url = finalQrCodes.get(position);
            intent.putExtra("title", title);
            intent.putExtra("url", url);
            intent.putExtra("user", user);
            startActivity(intent);
            finish();
        });

        scanButton.setOnClickListener(view -> {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(intent);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            }
        });

        generateButton.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), GenerateQRCodeActivity.class);
            intent.putExtra("user", user);
            startActivity(intent);
            finish();
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        String user = getIntent().getStringExtra("user");
        List<String> qrCodes = DBHelper.getAllQRCodesFromServer(user);
        List<String> qrTitles = DBHelper.getAllQRTitlesFromServer(user);
        ListAdapter adapter = new ListAdapter(this, qrCodes, qrTitles);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }




}