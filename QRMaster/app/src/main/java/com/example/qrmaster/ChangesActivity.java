package com.example.qrmaster;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ChangesActivity extends AppCompatActivity {

    Button editQRCodeButton, shareQRCodeButton, downloadQRCodeButton, goBackButton, deleteQRCodeButton;
    TextView textViewTitle, textViewUrl;
    ImageView imageViewQRCode;
    boolean isEditClicked = false;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changes);

        editQRCodeButton=findViewById(R.id.editQRCodeButton);
        shareQRCodeButton=findViewById(R.id.saveQRCodeButton);
        downloadQRCodeButton=findViewById(R.id.cancelQRCodeButton);
        goBackButton=findViewById(R.id.goBackButton);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewUrl=findViewById(R.id.textViewUrl);
        imageViewQRCode = findViewById(R.id.QRCodeview);
        deleteQRCodeButton = findViewById(R.id.deleteQRCodeButton);
        String title = getIntent().getStringExtra("title");
        String url = getIntent().getStringExtra("url");
        String user = getIntent().getStringExtra("user");
        textViewTitle.setText(title);
        textViewUrl.setText(url);
        generateQR();

        editQRCodeButton.setOnClickListener(view -> {
            isEditClicked = true;
            String newTitle =  textViewTitle.getText().toString();
            String newUrl = textViewUrl.getText().toString();
            DBHelper.updateQRTitle(newTitle, user,url);
            DBHelper.updateQRURL(newUrl, user,newTitle);
            generateQR();
            Toast.makeText(ChangesActivity.this, "QR code shared successfully", Toast.LENGTH_SHORT).show();
        });

        textViewTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isEditClicked) {
                    String newTitle = editable.toString();
                    DBHelper.updateQRTitle(newTitle, user,url);


                }
            }
        });

        textViewUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isEditClicked) {
                    String newUrl = editable.toString();
                    DBHelper.updateQRURL(newUrl, user,title);
                }
            }
        });

        shareQRCodeButton.setOnClickListener(view -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(ChangesActivity.this);
            builder.setTitle("Share QR Code");
            builder.setMessage("Enter the username of the user you want to share this QR Code with:");
            final EditText input = new EditText(ChangesActivity.this);
            builder.setView(input);
            builder.setPositiveButton("Share", (dialog, id) -> {
                String value = input.getText().toString();
                Boolean isUserInServer = DBHelper.checkUsernameInServer(value);
                Boolean insert = DBHelper.insertQRCodeIntoServer(value,title,url);
                if(isUserInServer && insert){
                    Toast.makeText(ChangesActivity.this, "QR code shared successfully", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(ChangesActivity.this, "user " + value + " not exist", Toast.LENGTH_SHORT).show();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        downloadQRCodeButton.setOnClickListener(view -> {
            bitmap = ((BitmapDrawable)imageViewQRCode.getDrawable()).getBitmap();
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "QR_Code.jpg");
            try {
                FileOutputStream fos = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(getApplicationContext(), "QR Code downloaded successfully!", Toast.LENGTH_SHORT).show();
        });

        deleteQRCodeButton.setOnClickListener(v -> {
            bitmap = ((BitmapDrawable)imageViewQRCode.getDrawable()).getBitmap();
            Boolean result = DBHelper.deleteQRCodeFromServer(title,url, user);
            if (result) {
                Toast.makeText(getApplicationContext(), "QR Code deleted successfully!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ChangesActivity.this, MainActivity.class);
                intent.putExtra("user", user);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to delete QR Code!", Toast.LENGTH_SHORT).show();
            }

        });

        goBackButton.setOnClickListener(view -> {

            Intent intent = new Intent(ChangesActivity.this, MainActivity.class);
            intent.putExtra("user", user);
            startActivity(intent);
            finish();
        });

    }

    private void generateQR(){
        String text = textViewUrl.getText().toString().trim();
        MultiFormatWriter writer = new MultiFormatWriter();
        try{
            BitMatrix matrix = writer.encode(text, BarcodeFormat.QR_CODE, 600, 600);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap =  encoder.createBitmap(matrix);
            imageViewQRCode.setImageBitmap(bitmap);
        }catch(WriterException | IllegalArgumentException e){
            e.printStackTrace();
        }
    }
}
