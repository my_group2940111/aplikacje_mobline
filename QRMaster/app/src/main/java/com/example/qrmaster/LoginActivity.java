package com.example.qrmaster;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    EditText username, password;
    Button loginConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username=findViewById(R.id.usernameLoginEditText);
        password=findViewById(R.id.passwordLoginEditText);
        loginConfirm=findViewById(R.id.LoginConfirmButton);

        loginConfirm.setOnClickListener(view -> {
            String user = username.getText().toString();
            String pass = password.getText().toString();
            if(TextUtils.isEmpty(user) || TextUtils.isEmpty(pass))
                Toast.makeText(LoginActivity.this, "All fields required", Toast.LENGTH_SHORT).show();
            else{
                Boolean checkUsernamePasswordInServer = DBHelper.checkUsernamePasswordInServer(user, pass);
                if(checkUsernamePasswordInServer){
                    Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("user", user);
                    startActivity(intent);

                }else{
                    Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}