package com.example.qrmaster;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class GenerateQRCodeActivity extends AppCompatActivity {

    Button generateQRCodeButton, saveQRCodeButton, goBackButton;
    ImageView iv_qr;
    EditText titleQRCode, urlQRCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_qr_code);

        saveQRCodeButton=findViewById(R.id.saveQRCodeButton);
        goBackButton=findViewById(R.id.goBackButton);
        generateQRCodeButton=findViewById(R.id.generateQRCodeButton);
        iv_qr=findViewById(R.id.iv_qr);
        titleQRCode=findViewById(R.id.titleQRCode);
        urlQRCode=findViewById(R.id.urlQRCode2);
        String user = getIntent().getStringExtra("user");


        generateQRCodeButton.setOnClickListener(view -> {
                 String title = titleQRCode.getText().toString();
                 String url = urlQRCode.getText().toString();
                    if (TextUtils.isEmpty(title) || TextUtils.isEmpty(url)) {
                        Toast.makeText(GenerateQRCodeActivity.this, "All fields required", Toast.LENGTH_SHORT).show();
                    } else {
                        generateQR();
                    }
                });



        saveQRCodeButton.setOnClickListener(view -> {
            String title = titleQRCode.getText().toString();
            String url = urlQRCode.getText().toString();
            if (TextUtils.isEmpty(title) || TextUtils.isEmpty(url)) {
                Toast.makeText(GenerateQRCodeActivity.this, "All fields required", Toast.LENGTH_SHORT).show();
            } else {
                Boolean insertQRCodeIntoServer = DBHelper.insertQRCodeIntoServer(user,title,url);
                Toast.makeText(GenerateQRCodeActivity.this, "QR code saved successfully", Toast.LENGTH_SHORT).show();
            }

        });

        goBackButton.setOnClickListener(view -> {
            Intent intent = new Intent(GenerateQRCodeActivity.this, MainActivity.class);
            intent.putExtra("user", user);
            startActivity(intent);
            finish();
        });
}

    private void generateQR(){
        String text = urlQRCode.getText().toString().trim();
        MultiFormatWriter writer = new MultiFormatWriter();
        try{
        BitMatrix matrix = writer.encode(text, BarcodeFormat.QR_CODE, 600, 600);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap =  encoder.createBitmap(matrix);
            iv_qr.setImageBitmap(bitmap);
    }catch(WriterException e){
            e.printStackTrace();
        }
}}
