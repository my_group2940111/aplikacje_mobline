package com.example.qrmaster;

import static org.junit.Assert.*;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.test.InstrumentationRegistry;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ListAdapterTest {
    @Test
    public void testListAdapterGetView() {
        List<String> qrCodes = Arrays.asList("qr1", "qr2");
        List<String> qrTitles = Arrays.asList("title1", "title2");
        Context context = InstrumentationRegistry.getTargetContext();

        ListAdapter listAdapter = new ListAdapter(context, qrCodes, qrTitles);
        View rowView = listAdapter.getView(0, null, null);
        TextView textViewQrCode = (TextView) rowView.findViewById(R.id.urlCode);
        TextView textViewQrTitle = (TextView) rowView.findViewById(R.id.titleCode);

        assertEquals("qr1", textViewQrCode.getText());
        assertEquals("title1", textViewQrTitle.getText());
    }

}