package com.example.qrmaster;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ChangesActivityTest {
    @Rule
    public ActivityScenarioRule<ChangesActivity> activityScenarioRule = new ActivityScenarioRule<>(ChangesActivity.class);

    @Test
    public void  checkTitleAvailability(){
        onView(withId(R.id.textViewTitle)).check(matches(isDisplayed()));
    }

    @Test
    public void checkUrlAvailability(){
        onView(withId(R.id.textViewUrl)).check(matches(isDisplayed()));
    }
}