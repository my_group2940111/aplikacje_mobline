package com.example.qrmaster;

import static org.junit.Assert.*;

import org.junit.Test;

import java.sql.Connection;

public class ConnectToServerTest {
    @Test
    public void testConnectToServer() {
        ConnectToServer connectToServer = new ConnectToServer();
        Connection conn = connectToServer.doInBackground();

        assertNotNull(conn);
    }

}