package com.example.qrmaster;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import java.util.List;

public class DBHelperTest {

    @Test
    public void testInsertDataIntoServer() {
        String username = "testUser";
        String email = "test@example.com";
        String password = "password";
        String repassword = "password";

        Boolean result = DBHelper.insertDataIntoServer(username, email, password, repassword);
        assertTrue(result);
    }

    @Test
    public void testCheckUsernameInServer() {
        String username = "testUser";

        Boolean result = DBHelper.checkUsernameInServer(username);
        assertTrue(result);
    }

    @Test
    public void testCheckUsernamePasswordInServer() {
        String username = "testUser";
        String password = "password";

        Boolean result = DBHelper.checkUsernamePasswordInServer(username, password);
        assertTrue(result);
    }

    @Test
    public void testInsertQRCodeIntoServer() {
        String username = "testUser";
        String title = "testTitle";
        String qrcode = "testQRCode";

        Boolean result = DBHelper.insertQRCodeIntoServer(username, title, qrcode);
        assertTrue(result);
    }

    @Test
    public void testDeleteQRCodeFromServer() {
        String username = "testUser";
        String title = "testTitle";
        String qrcode = "testQRCode";
        Boolean insert = DBHelper.insertQRCodeIntoServer(username, title, qrcode);

        Boolean result = DBHelper.deleteQRCodeFromServer(title, qrcode, username);
        assertTrue(result);
    }

    @Test
    public void testUpdateQRURL() {
        String qrurl = "testURL";
        String user = "testUser";
        String title = "testTitle";

        DBHelper.updateQRURL(qrurl, user, title);
        //Sprawdź, czy adres URL został poprawnie zaktualizowany.
    }

    @Test
    public void testUpdateQRTitle() {
        String qrtitle = "testTitle";
        String user = "testUser";
        String qrcode = "testQRCode";

        DBHelper.updateQRTitle(qrtitle, user, qrcode);

    }
    @Test
    public void testGetAllQRCodesFromServer() {
        String username = "testUser";

        List<String> qrCodes = DBHelper.getAllQRCodesFromServer(username);
        assertNotNull(qrCodes);
    }

    @Test
    public void testGetAllQRTitlesFromServer() {
        String username = "testUser";

        List<String> titles = DBHelper.getAllQRTitlesFromServer(username);
        assertNotNull(titles);
    }


}