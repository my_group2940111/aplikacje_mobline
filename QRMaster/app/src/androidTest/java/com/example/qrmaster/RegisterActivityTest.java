package com.example.qrmaster;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class RegisterActivityTest {
    @Rule
    public ActivityScenarioRule<RegisterActivity> activityScenarioRule = new ActivityScenarioRule<>(RegisterActivity.class);

    @Test
    public void  checkNameAvailability(){
        onView(withId(R.id.usernameEditText)).check(matches(isDisplayed()));
    }

    @Test
    public void checkEmailAvailability(){
        onView(withId(R.id.emailEditText)).check(matches(isDisplayed()));
    }

    @Test
    public void  checkPasswordAvailability(){
        onView(withId(R.id.passwordEditText)).check(matches(isDisplayed()));
    }

    @Test
    public void checkRePasswordAvailability(){
        onView(withId(R.id.rePasswordEditText)).check(matches(isDisplayed()));
    }
}